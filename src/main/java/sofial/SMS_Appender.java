package sofial;

import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;


@Plugin(name = "SMS",category = "Core",elementType = "appender",printObject = true )
public final class SMS_Appender extends AbstractAppender {

    protected SMS_Appender(String name, Filter filter, Layout<?extends Serializable> layout,final boolean ignoreExceptions){
        super(name, filter, layout, ignoreExceptions);
    }

    @Override
    public void append(LogEvent event) {
        try {
            SMS_Sender.send(new String(getLayout().toByteArray(event)));

        }catch (Exception ex){

        }

    }

    @PluginFactory
    public static SMS_Appender createAppender(@PluginAttribute("name")String name,
                                             @PluginElement("Layout")Layout<?extends Serializable>layout,
                                             @PluginElement("Filter")final Filter filter,
                                             @PluginAttribute("otherAttribute")String otherAttribute){
        if (name == null){
            LOGGER.error("No name provided");
            return null;
        }
        if(layout == null){
            layout = PatternLayout.createDefaultLayout();

        }
        return new SMS_Appender(name,filter,layout,true);
    }
}